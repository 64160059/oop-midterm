package com.sasipa.midterm;

public class Garage {
        private String name;
        private int hour;
        private int price;
        
    
        public Garage(String name, int price , int hour) {
            this.name = name;
            this.hour = hour;
            this.price = price;
        }

        public String getName() {
            return name;
        }
    
        public int getPrice() {
            return price;
    
        }
    
        public int getHour() {
            return hour;
            
        }

        public void print() {
            System.out.println();
        }
    }


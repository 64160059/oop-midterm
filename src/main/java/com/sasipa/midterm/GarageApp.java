package com.sasipa.midterm;

import java.util.Scanner;

public class GarageApp {
    static void printgarage() {
        System.out.println("garage class");
        System.out.println("choice 1 premiumfloorgarage1");
        System.out.println("choice 2 vipfloorgarage2");
        System.out.println("choice 3 standardfloorgarage3");
        System.out.println("choice 4 exit");
    }

    public static void main(String[] args) {
        printgarage();
        Scanner sc = new Scanner(System.in);
        Garage premiumfloorgarage1 = new Garage("premiumfloorgarage1", 200, 1);
        Garage vipfloorgarage2 = new Garage("vipfloorgarage2", 100, 1);
        Garage standardfloorgarage3 = new Garage("standardfloorgarage3", 20, 1);
        System.out.print("Please choice garage = ");
        int choice = sc.nextInt();
        int price;
        {
                if (choice == 1) {
                    System.out.print("How many hour : ");
                    int hour = sc.nextInt();
                    if (hour == 1)
                        price = 200;
                    System.out.println("price = " + premiumfloorgarage1.getPrice() * hour + " Baht ");
                } else if (choice == 2) {
                    System.out.print("How many hour : ");
                    int hour = sc.nextInt();
                    if (hour == 1)
                        price = 100;
                    System.out.println("price = " + vipfloorgarage2.getPrice() * hour + " Baht ");
                } else if (choice == 3) {
                    System.out.print("How many hour : ");
                    int hour = sc.nextInt();
                    if (hour == 1)
                        price = 20;
                    System.out.println("price = " + standardfloorgarage3.getPrice() * hour + " Baht ");
                } else if (choice == 4) {
                    System.exit(0);
                }
        }
    }
}

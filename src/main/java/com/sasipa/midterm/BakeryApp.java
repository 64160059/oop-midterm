package com.sasipa.midterm;

import java.util.Scanner;

public class BakeryApp {
    static void printmenu() {
        System.out.println("---Menu---");
        System.out.println("push 1 Cheese pie");
        System.out.println("push 2 Brownies");
        System.out.println("push 3 Croissant");
        System.out.println("push 4 Cheesecake");
        System.out.println("push 5 chocolate cake");
        System.out.println("push 6 Exit Menu");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Bakery cheesepie = new Bakery("Cheese pie", 80, 20);
        Bakery brownies = new Bakery("Brownies", 50, 20);
        Bakery croissant = new Bakery("Croissant", 40, 20);
        Bakery cheesecake = new Bakery("Cheesecake", 65, 20);
        Bakery chocolatecake = new Bakery("chocolate cake", 45, 20);

        while (true) {
            printmenu();
            System.out.print("Please put menu number : ");
            int number = sc.nextInt();
            if (number == 1) {
                System.out.print("How many pieces : ");
                int pieces = sc.nextInt();
                cheesepie.sellBakery(pieces);
                if (cheesepie.sellBakery(pieces)) {
                    System.out.println("price = " + cheesepie.getPrice() * pieces);
                }else{
                    System.out.println(cheesepie.getName() + " not enough ");
                }

            } else if (number == 2) {
                System.out.print("How many pieces : ");
                int pieces = sc.nextInt();
                brownies.sellBakery(pieces);
                if (brownies.sellBakery(pieces)) {
                    System.out.println("price = " + brownies.getPrice() * pieces);
                }else{
                    System.out.println(brownies.getName() + " not enough ");
                }

            } else if (number == 3) {
                System.out.print("How many pieces : ");
                int pieces = sc.nextInt();
                croissant.sellBakery(pieces);
                if (croissant.sellBakery(pieces)) {
                    System.out.println("price = " + croissant.getPrice() * pieces);
                }else{
                    System.out.println(croissant.getName() + " not enough ");
                }

            } else if (number == 4) {
                System.out.print("How many pieces : ");
                int pieces = sc.nextInt();
                cheesecake.sellBakery(pieces);
                if (cheesecake.sellBakery(pieces)) {
                    System.out.println("price = " + cheesecake.getPrice() * pieces);
                }else{
                    System.out.println(cheesecake.getName() + " not enough ");
                }

            } else if (number == 5) {
                System.out.print("How many pieces : ");
                int pieces = sc.nextInt();
                chocolatecake.sellBakery(pieces);
                if (chocolatecake.sellBakery(pieces)) {
                    System.out.println("price = " + chocolatecake.getPrice() * pieces);
                }else{
                    System.out.println(chocolatecake.getName() + " not enough ");
                }

            } else if (number == 6) {
                System.exit(0);
            }
        }
    }
}

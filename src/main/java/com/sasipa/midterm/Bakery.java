package com.sasipa.midterm;

public class Bakery {
    private String name;
    private int price;
    private int pieces;

    public Bakery(String name, int price, int pieces) {
        this.name = name;
        this.price = price;
        this.pieces = pieces;

    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getPieces() {
        return pieces;
    }

    public void print() {
        System.out.println(name + " " + pieces);

    }

    public boolean sellBakery(int num) {
        if (pieces >= num) {
            pieces = pieces - num;
            return true;
        } else {
            return false;


        }

    }

}
